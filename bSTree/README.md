# **Binary Search Tree**
La consegna prevede la realizzazione di un albero binario, con i seguenti metodi:
* Metodo di inserimento
* Metodo di ricerca
* Metodo di cancellazione
#
### **Struttura dell'albero**
L'albero è strutturato da una matrice bidimensionale, la quale contiene la numerazione dei nodi ed i rispettivi valori.<br>
Ogni nodo viene inizializzato con numerazione `-1` e valore `0`, necessaria per identificare i nodi non esistenti.
### **Metodo di inserimento**
Per l'inserimento di un valore all'albero è sufficiente utilizzare il metodo `insert(valore)`, i figli di un nodo vengono inseriti rispettivamente<br>
alla posizione: `nodoGenitore*2+1` nel caso di un figlio destro; mentre `nodoGenitore*2+2` nel caso di un figlio sinistro. chiaramente l'array viene ridimensionato<br>
dinamicamente in base alla necessità, lasciando però dei nodi nulli.<br>
### **Metodo di ricerca**
La ricerca di un nodo può essere effettuata in due modalità: `BFS` e `DFS`<br>
* **BFS**:  `Breadth First Search`, da non confondere con `BSF`&#8594; `Buéo sensa fondo`, é un algoritmo di ricerca che esamina in ordine tutti i nodi<br>
di ogni livello sistematicamente, partendo dalla radice. Nel metodo `BFS()` viene richiamato il metodo `clearTree(matriceNodi)`, il quale<br>
prepara la matrice da esaminare senza i nodi non esistenti, risparmiando molta memoria RAM qualora si sostituisse direttamente la matrice<br> 
principale con quella ridotta, cosa che non accade in questo caso poiché la matrice con i nodi non esistenti viene utilizzata per la ricerca `BFS`. 
<br></br>
* **DFS**: `Depth First Search` é un algoritmo di ricerca che esamina l'albero in profondità sfruttando la suddivisione dei dati: In questo caso<br>
i nodi con valore maggiore a quello del nodo genitore vengono posizionati a destra, mentre quelli con valore minore a sinistra.<br>
Il metodo `DFS()` effettua la ricerca senza eliminare i nodi non esistenti, poiché calcola la posizione del nodo per viaggiare nel vettore più velocemente.
### **Metodo di cancellazione**
Il metodo di cancellazione `delete(nodo)` risulta essere più complicato e si divide in tre casistiche:
* **Foglia**: Nel caso il nodo da eliminare sia una foglia la pratica da eseguire é molto semplice: Eliminare il nodo. nella struttura di questo albero per
eliminare un nodo é sufficiente assegnargli il valore di inizializzazione, ovvero numerazione nodo `-1` e valore nodo `0`.
    ```
              50                            50
           /     \         delete(20)      /   \
          30      70       --------->    30     70 
         /  \    /  \                     \    /  \ 
       20   40  60   80                   40  60   80
    ```
* **Un solo figlio**: Se il nodo da eliminare possiede solo un figlio, anziché reinizializzare il nodo é unicamente necessario sostituirlo con il suo unico figlio.
La procedura consiste nel spostare tutti i nodi successivi a quello da eliminaire indietro di un livello, per realizzarla é stato utilizzato il metodo ricorsivo `shifting`.<br>
    ```
              50                            50
           /     \         delete(30)      /   \
          30      70       --------->    40     70 
            \    /  \                          /  \ 
            40  60   80                       60   80
    ```
* **Due figli**: Nel caso si debba eliminare un nodo con due figli la tecnica cambia ancora: Il nodo viene sostituito con il più piccolo valore trovato dal
figlio destro del nodo in poi; Mi spiego peggio: A partire dal figlio destro (valore maggiore rispetto il nodo padre) del nodo da eliminare viene cercata la foglia più a sinistra (ovvero valore minore), sostituendo il nodo da cancellare con il nodo trovato.
    ```
              50                            60
           /     \         delete(50)      /   \
          40      70       --------->    40    70 
                 /  \                            \ 
                60   80                           80
    ```
