package com.company;

public class Main {
    public static void main(String[] args){
        BSTree tree = new BSTree(15);
        tree.generateTree();
        tree.insert(44);
        tree.printTree();
        tree.bfs(44);
        tree.dfs(44);
        tree.delete(44);
        tree.printTree();


    }
}
