package com.company;
import java.util.Random;

public class BSTree {
    private int[][] nodes;
    private int n;

    BSTree(int n){
        this.n = n;
        this.nodes = new int[2][n];
    }

    public void generateTree(){ // Generate a tree with random N nodes
        Random rng = new Random();
        nodesInitialization();
        nodes[0][0] = 0;
        nodes[1][0] = rng.nextInt(100)+1;
        for(int i = 0; i < n; i++){
            insert(rng.nextInt(100)+1);
        }
    }

    public void printTree(){ // Print the tree resized
        int [][] tree = clearTree(nodes);
        for(int i =0; i < tree.length; i++){
            for(int j = 0; j<tree[0].length; j++){
                System.out.print(tree[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public void insert(int val) { // Inserts a node
        for (int i = 0; i < nodes[1][i] || i >= nodes[1][i];) {
            if (val < nodes[1][i]) {
                i = i * 2 + 1;
            } else {
                i = i * 2 + 2;
            }
            if(i>=nodes[0].length) {
                int[][] cpNodes = nodes;
                nodes = new int[2][i + 1];
                nodesInitialization();
                for (int j = 0; j < cpNodes[0].length; j++) {
                    nodes[0][j] = cpNodes[0][j];
                    nodes[1][j] = cpNodes[1][j];
                }
            }
            if(nodes[0][i] == -1){
                nodes[1][i] = val;
                nodes[0][i] = i;
                break;
            }
        }
    }

    public void delete(int val){
        for(int i = 0; i<nodes[0].length; i++){
            if(nodes[1][i] == val){
                System.out.println("deleted node: " +  nodes[0][i] + ", value:  " +  nodes[1][i]);
                if(i*2+3 >= nodes[0].length ){ // If is a leaf
                    nodes[0][i] = -1;
                    nodes[1][i] = 0;
                    break;
                } else if (nodes[0][i*2+1] == -1 || nodes[0][i*2+2] == -1) { // If there is one child
                    if(nodes[0][i*2+1] == -1){
                        nodes[1][i] = nodes[1][i*2+2];
                        shifting(i*2+2, -1);
                    } else{
                        nodes[1][i] = nodes[1][i*2+1];
                        shifting(i*2+1, -1);
                    }
                } else if(nodes[0][i*2+1] != -1 && nodes[0][i*2+2] != -1){ // If there are two child
                    for(int j = i*2+2; j*2+1<nodes[0].length; j=j*2+1){
                        if(nodes[0][j*2+1] == -1){
                            nodes[1][i] = nodes[1][j];
                            nodes[1][j] = 0;
                            nodes[0][j] = -1;
                            break;
                        }
                    }
                }
            }
        }
    }

    public void shifting(int j, int pos){
        if (j*2+2 < nodes[0].length){
            nodes[1][j] = nodes[1][j*2+1];
            nodes[1][j+pos] = nodes[1][j*2+1];
            shifting(j*2+1, 1);
            shifting(j*2+2, -1);
        }
    }

    public void bfs(int val){ // Breadth First Search, no Bueo Sensa Fondo
        int [][] matrix = clearTree(nodes);
        boolean found = false;
        for(int i = 0; i < matrix[0].length; i++){
            if (matrix[1][i] == val){
                found = true;
                System.out.println("bfs: found node: " + matrix[0][i] + ", value: " + val);
            }
        }
        if (!found){
            System.out.println("node not found!");
        }
    }

    public void dfs(int val){ // Depth First Search
        for (int i = 0; i < nodes[1][i] || i >= nodes[1][i];){
            if (val < nodes[1][i]) {
                i = i * 2 + 1;
            } else {
                i = i * 2 + 2;
            }
            if (val == nodes[1][i]){
                System.out.println("dfs: found node: " + nodes[0][i] + ", value: " + val);
                break;
            }
            if(i>=nodes[0].length) {
                System.out.println("node not found!");
                break;
            }
        }
    }

    private int[][] clearTree(int[][] m){ // Resize the array discarding not existing nodes
        int[][] matrix = new int[2][m[0].length];
        for(int i = 0; i < matrix[0].length; i++){
            matrix[0][i] = m[0][i];
            matrix[1][i] = m[1][i];
        }
        int size = 0;
        for(int i = 0; i < matrix[0].length;  i++){
            clear(-1, matrix);
        }
        for(int i = 0; i < matrix[0].length-1 && matrix[0][i] != -1 && matrix[0][i] != matrix[0][i+1];  i++){
            size++;
        }
        int[][] cpNodes = matrix;
        matrix = new int[2][size+1];
        for (int j = 0; j < size+1; j++) {
            matrix[0][j] = cpNodes[0][j];
            matrix[1][j] = cpNodes[1][j];
        }
        return matrix;
    }

    private void clear(int val, int[][] matrix){  // Shifting -1
        for(int i = 0; i < matrix[0].length; i++){
            if(matrix[0][i] == val || matrix[1][i] == 0){
                for(int j = i; j < matrix[0].length-1; j++){
                    matrix[0][j] = matrix[0][j+1];
                    matrix[1][j] = matrix[1][j+1];
                }
            }
        }
    }

    private void nodesInitialization(){ // Initializes not existing nodes with value -1
        for (int j = 0; j < nodes[0].length; j++) {
            nodes[0][j] = -1;
        }
    }
}
