package com.company;

import java.util.Random;

public class MyGraph {
    public int[][] graph;

    static MyGraph getGraph(int n) {
        MyGraph g = new MyGraph();
        g.graph = new int[n][n];
        return g;
    }

    void testOutput() {
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                System.out.print(graph[i][j] + " ");
            }
            System.out.println();
        }
    }

    void initialize() {
        Random rng = new Random();
        for (int i = 0; i < 6; i++) {
            int c = 0;
            for (int j = 0; j < 6; j++) {
                if (rng.nextBoolean() && c < 3) {
                    graph[i][j] = rng.nextInt(9) + 1;
                    c++;
                }
            }
            graph[i][i] = 0; // elimina i cappi
        }
    }

    static class PriorityQueue {
        public int[][] queue;

        PriorityQueue(int n) {
            this.queue = new int[2][n];
        }

        public void initilizeQueue() {
            for (int i = 0; i < queue[0].length; i++) {
                queue[0][i] = i;
                queue[1][i] = Integer.MAX_VALUE;
            }
            queue[1][0] = 0;
        }

        public void dijkstra(MyGraph g) {
            for (int i = 0; i < queue[1].length; i++) {
                for (int j = 0; j < g.graph[0].length; j++) {
                    for (int k = 1; k < queue[1].length; k++) {
                        if (queue[0][k] == j && g.graph[queue[0][i]][j] !=0) { // ricerca nodo puntato e corrispettivo rilassamento
                            if(queue[1][i] != Integer.MAX_VALUE){
                                if (g.graph[queue[0][i]][j] + queue[1][i] < queue[1][k]) {
                                    queue[1][k] = g.graph[queue[0][i]][j] + queue[1][i];
                                }
                            } else if (g.graph[queue[0][i]][j] + queue[1][i] < queue[1][k]){
                                queue[1][k] = g.graph[queue[0][i]][j];
                            }
                            sortQueue();
                        }
                    }
                }
            }
        }

        public void sortQueue(){
            for (int j = 0; j < queue[0].length; j++) {
                for (int i = 1; i < queue[0].length; i++) {
                    if(queue[1][i] < queue[1][i-1]){
                        int nodo = queue[0][i];
                        int marcatura = queue[1][i];
                        queue[0][i] = queue[0][i-1];
                        queue[1][i] = queue[1][i-1];
                        queue[0][i-1] = nodo;
                        queue[1][i-1] = marcatura;
                    }
                }
            }
        }

        public void printQueue(){
            System.out.println();
            for (int i = 0; i < queue[0].length; i++) {
                System.out.print(queue[0][i]+1 + "\t");
            }
            System.out.println();
            for (int i = 0; i < queue[0].length; i++) {
                System.out.print(queue[1][i] + "\t");
            }
        }
    }
}
