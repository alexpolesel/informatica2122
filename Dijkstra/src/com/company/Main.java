package com.company;

public class Main {

    public static void main(String[] args) {
        MyGraph g = MyGraph.getGraph(7);
        g.initialize();
        g.testOutput();

        MyGraph.PriorityQueue q = new MyGraph.PriorityQueue(6);
        q.initilizeQueue();
        q.dijkstra(g);
        q.printQueue();
    }
}
