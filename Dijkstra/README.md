### **Dijkstra**
Applicazione dell'algoritmo di [Dijkstra](https://it.wikipedia.org/wiki/Algoritmo_di_Dijkstra) in java.

* Struttura dati: Per rappresentare il grafo viene utilizzata una matrice, la quale rapppresenta le adiacenze dei nodi (orientate). Inoltre viene utilizzata un'altra matrice per rappresentare la coda di priorita', fondamentale per l'algoritmo.
Esempio:
```
Matrice             Coda
0 0 4 5 0 0     Nodi: 1	3 4 6 5	2
0 0 0 0 2 0     Peso: 0	4 5 6 7	11
0 7 0 0 0 2 
0 0 0 0 2 0 
0 5 0 2 0 0 
7 0 5 0 0 0 

```

