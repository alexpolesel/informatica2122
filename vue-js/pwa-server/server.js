const axios = require('axios');
const express = require('express');
const fs = require('fs');
const cors = require('cors');
var bodyParser = require('body-parser');
const { get } = require('http');
const { getMaxListeners } = require('process');
const app = express();
const port = 3000;

app.use(cors());
app.use(express.json())
app.use(bodyParser.urlencoded({
  extended: true
}));

const api = "https://api.steampowered.com/ISteamApps/GetAppList/v2/";
const urlPrices = "https://store.steampowered.com/api/appdetails?filters=price_overview&appids=";

let wishlist = JSON.parse(fs.readFileSync('wishlist.json'));

console.log(wishlist)

app.listen(port, () => {
  console.log(`server listening on port ${port}`);
})

app.get('/getwishlist', (req, res) => {
  console.log(wishlist)
  res.send(wishlist);
})

app.get('/getgames', (req, res) => {
  let gameList = axios.get(api)
    .then((response) => {
      gameList = response.data;
      res.send(gameList);
    1});
  
})

app.post('/updatewishlist',(req,res)=>{
  wishlist = req.body.wishlist;
  fs.writeFileSync('wishlist.json', JSON.stringify(wishlist));
})

app.post('/addtowishlist', async (req, res) => {
  let id = req.body.id;
  let name = req.body.name;
  let index = req.body.index;
  let final = await getGamePrice(id).then((price_overview)=>{return price_overview.final_formatted})
  let initial = await getGamePrice(id).then((price_overview)=>{return price_overview.initial_formatted})
 
  wishlist.push({
    name: name,
    currentPrice: final,
    olderPrice: initial,
    id: id,
    index: index
  });

  console.log(wishlist)
  fs.writeFileSync('wishlist.json', JSON.stringify(wishlist));
  res.end("yes");
  });

  function getGamePrice(appid){ // to fix
    let url = urlPrices+appid;
    const promise = axios.get(url)
    const dataPromise = promise.then((response)=>{
    let info = JSON.parse(JSON.stringify(response.data).replace(appid, "game"))
    return info.game.data.price_overview
    })
    promise.catch(function (error) {
        console.log(error);
    });
    return dataPromise;
  }