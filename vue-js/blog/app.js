

const Blog = {
    data() {
        return {
            posts: [{
                    index: 0,
                    title: "Lockheed SR-71",
                    body: "Il Lockheed SR-71, meglio conosciuto con il soprannome non ufficiale Blackbird, era un ricognitore strategico statunitense che prestò servizio dal 1966 fino al 22 novembre 1989, anno in cui tutti gli esemplari furono dismessi a causa della soppressione dei fondi per il loro utilizzo. Durante la sua carriera, l'SR-71 ha infranto alcuni record, ad oggi rimasti imbattuti, in particolare il primato, dal 1976, per la maggior velocità mai raggiunta da un aereo con pilota (3530 km/h) e l'altezza massima (quasi 26000 m).",
                    category: "Aircraft",
                    comments: ["ottimo veivolo", "bel mezzo"],
                    tags: ["lockheed", "ricognitore"],
                    show: true
                    },
                    {
                    index: 1,
                    title: "Sukhoi Su-57",
                    body: "Il Sukhoi Su-57 (in cirillico: Сухой Су-57, nome in codice NATO: Felon) noto anche come T-50 durante la fase di sviluppo, è un caccia multiruolo stealth di fabbricazione russa sviluppato dalla Sukhoi negli anni duemiladieci ed entrato in servizio nelle forze aerospaziali russe a partire dal 2020. Progettato per conquistare la superiorità aerea sull'avversario nonché condurre missioni di attacco al suolo, il velivolo è in grado di operare in modalità autonoma, di coordinare l'operato dei propri gregari e di interfacciarsi attivamente con droni da combattimento al fine di potenziare lo spettro dei propri sensori; è altresì il primo velivolo di 5ª generazione realizzato in Russia. In precedenza identificato con l'acronimo PAK-FA, dall'omonimo programma da cui ha avuto origine, ha acquisito ufficialmente la denominazione Su-57 nell'agosto del 2017 ed è stato testato nei cieli della Siria durante la guerra civile siriana. Al 2020 ne è in corso l'integrazione con il drone da combattimento pesante S-70 Okhotnik e ne è stata realizzata una versione da esportazione denominata Su-57E.",
                    category: "Aircraft",
                    comments: ["bastardi russi", "ganzo eh pero'", "spero di non trovarmelo mai sopra casa"],
                    tags: ["fighter"],
                    show: true

                    },
                    {
                    index: 2,
                    title: "Colt M1911",
                    body: "La Colt M1911 (detta anche Colt .45 per via del tipo di munizionamento), spesso chiamata 1911, è una pistola semiautomatica ad azione singola calibro .45 ACP. Progettata da John Browning, è stata la pistola d'ordinanza delle United States Armed Forces dal 1911 al 1985. Venne inoltre largamente usata nella prima e nella seconda guerra mondiale, in Corea e in Vietnam. In totale, nelle versioni M1911 e M1911A1, ne sono stati prodotti 2 700 000 esemplari.",
                    category: "Weapon",
                    comments: ["urca", "la hanno anche gli zingari"],
                    tags: ["guns", "colt"],
                    show: true
                    
                    },
                    {
                    index: 3,
                    title: "Lockheed A-12",
                    body: "Il Lockheed A-12 (nome in codice OXCART) era un aereo militare da ricognizione prodotto dalla sezione Skunk Works della Lockheed Corporation per conto della CIA, sulla base dei disegni di Clarence \"Kelly\" Johnson. Sviluppato tra il 1962 ed il 1964 e operativo dal 1963 al 1968, è considerato il precursore di altri due aeroplani da ricognizione prodotti dalla Lockheed e poi utilizzati dalla USAF, il caccia intercettore YF-12 ed il famoso SR-71 Blackbird. Partecipò a diverse missioni legate all'operazione Black Shield nel corso della Guerra in Vietnam, l'ultima delle quali venne effettuata nel maggio del '68 ed il programma A-12 terminò nel giugno dello stesso anno. Pare sia stato, anche se meno conosciuto, un aereo che ha più volte avuto la capacità di viaggiare a velocità più elevate del successivo SR-71, arrivando a Mach 3.35 contro i 3.2 del famoso ricognitore.",
                    category: "Aircraft",
                    comments: ["dove lo posso comprare?", "poderoso"],
                    tags: ["lockheed", "ricognitore"],
                    show: true
    
                    },
                    
                    
                ],
            addedTags: [],
        }
    },
    
    methods: {
        addPost(title, body , category) {
            if (title.length == 0) {this.addedTags = []; return; }
            if (category.length == 0){ this.addedTags = []; return; }
            this.posts.push({ index: this.posts.length,title: title, body: body, category: category, comments: [], tags: this.addedTags, show: true});
            this.addedTags = [];
            },
        addComment(index, text){
            if (text.length == 0) return;
            this.posts[index].comments.push(text);
        },
        addTag(tag){
            if (tag.length == 0) return;
            console.log(this.addedTags);
            this.addedTags.push(tag);
        },
        deleteAll() {
            this.posts = [];
        },
        deletePost(data) {
            console.log("delete: " + data);
            this.posts.splice(data, 1);
        },
        searchByTag(tag){
            for (let i = 0; i < this.posts.length; i++) {
                this.posts[i].show = false;
                for (let j = 0; j < this.posts[i].tags.length; j++) {
                    if(this.posts[i].tags[j] == tag){
                        this.posts[i].show = true;
                    }
                }
            }
        },
        searchByCategory(category){
            console.log(category);
            for (let i = 0; i < this.posts.length; i++) {
                if(category != "All"){
                    this.posts[i].show = false;
                } else {
                    this.posts[i].show = true;
                }
                if(this.posts[i].category == category){
                    this.posts[i].show = true;
                }
            }
        },

        filtersOff(){
            for (let i = 0; i < this.posts.length; i++) {
                this.posts[i].show = true;
            }
        }
    }
}
  
const app = Vue.createApp(Blog);

app.component('post', {
    props: {
        index: Number,
        title: String,
        body: String,
        category: String,
        comments: String, Array,
        tags: String, Array,
        show: Boolean
    },
    methods: {
        deletePost(index){
            this.$emit('deletePost', index);
        },
        addComment(index, text){
            this.$emit('addComment', index, text);
        }
        
     },
    template:
        `
            <div v-show="show" class="container p-3 my-3 bg-dark text-white rounded">
            <span class="badge badge-danger">{{category}}</span> | 
            <span v-for="(tag, index) in tags" :key="tag">
                <span class="badge badge-danger mx-1">{{tag}}</span>
            </span>
            <span>
            <button v-on:click="deletePost(index)" class="btn btn-outline-danger float-right">delete</button>    
            </span>
            <h1>{{title}}</h1> 
            <article>{{body}}</article>
            </br>
            <li v-for="(comment, index) in comments" :key="comment" style="list-style-type:none;" class="my-2">
                
                <span class="badge badge-warning ">Anonymous: </span>
                
                    <span>
                        <div class="bg-secondary w-50 p-3 text-light  my-2">
                        {{comment}}
                        </div>
                    </span>
                
            </li>
            <li>
            <div class="input-group w-50">
            <input v-model="comment" type="text" placeholder="add a comment" class="form-control"  />
            <button @click="addComment(index, comment), comment = ''" class="btn btn-outline-danger mx-1" >add</button>
            </div>
            </li>
        </div>
        `
    
});

app.mount('#blog');