
const Calculator = {
    data() {
        return {
            expression: " ",
            result: 0,
            number: 0,
        }
    },
    mounted() {
        
    },

    methods: {

        // Basic Operations
        plus(){
            this.expression = this.expression + "+";
        },
        
        minus(){
            this.expression = this.expression + "-";

        },

        times(){
            this.expression = this.expression + "*";        
        },

        divideBy(){
            this.expression = this.expression + "/";
        },

        reset(){
            this.expression = " ";
        },

        equals(){
            this.expression = eval(this.expression).toString();
        },

        // input
        set(x){
            this.number = x;
            this.expression = this.expression + x.toString();
        }
    }
}
  
Vue.createApp(Calculator).mount('#calculator')